const { User } = require("../models");
const bcrypt = require("bcrypt");

module.exports = {
  register(data) {
    const hashPassword = bcrypt.hashSync(data.password, 10);
    data.password = hashPassword;
    // console.log(data, 'bcrypt');
    return User.create(data);
  },
  findByEmail(email) {
    return User.findOne ({
      where: { email }
    });
  },
  // update(id, updateArgs) {
  //   return Post.update(updateArgs, {
  //     where: {
  //       id,
  //     },
  //   });
  // },

  // delete(id) {
  //   return Post.destroy(id);
  // },

  // find(id) {
  //   return Post.findByPk(id);
  // },

  // findAll() {
  //   return Post.findAll();
  // },

  // getTotalPost() {
  //   return Post.count();
  // },
};
