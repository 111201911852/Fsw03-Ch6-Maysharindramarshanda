const jwt = require('jsonwebtoken');
const postRepository = require('../repositories/postRepository');

module.exports = {
    //AUTHORIZE
    async checkToken(req,res,next) {
        try {
            const beererToken = req.headers.authorization;
            const token = beererToken.split('Bearer ')[1];
            const payload = jwt.verify(token, process.env.JWT_SECRET);
            
            req.user = await postRepository.findByEmail(payload.email)
        } catch (error) {
            res.status(400).json({
                status: "FAIL",
                message: error.message,
            });
        }
    }
}